## Task Tracker Project - Backend


>This project is configured to run with docker

Core Dependency: **Docker, docker-compose**

>  Tech Stack

Django 2.1
Python 3.7
Db Sqlite3

>To Build Project: 

```docker-compose build```

>To Run Project: 

```docker-compose up```

> Data Workflow

Request <=> Url Dispatcher <=> Controller <=> Repository <=> Model <=> Db 

### Internal Project Url Shortener

>To generate Short URL use curl
``curl -X POST \
  http://0.0.0.0:8000/shorten_url \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d long_url=http%3A%2F%2Fwww.google.com``

