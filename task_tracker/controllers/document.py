from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
from task_tracker.repositories.document import *
from task_tracker.helpers.base_utils import JSONResponse


class DocumentController:

    @staticmethod
    @api_view(['POST'])
    @login_required
    def upload_document(request):
        post_data = request.data
        document = post_data.get('file')
        task_id = post_data.get('taskId')
        status, message = DocumentRepository.create_document(
            type = 'TASK',
            type_id = task_id,
            user = request.user,
            file = document,         
        )
        if status:
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': 'Bad Request'})
        
    @staticmethod
    @api_view(['DELETE'])
    @login_required
    def delete_document(request, document_id):
        status, message = DocumentRepository.delete_document(document_id)
        if status:
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': 'Bad Request'})

        