from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from rest_framework.decorators import api_view
from rest_framework import authtoken	

from task_tracker.helpers.base_utils import *
from task_tracker.repositories.user import *
from task_tracker.serializer import *

import logging

logger = logging.getLogger()

class UserController:

    @staticmethod
    @api_view(['POST'])
    def signup(request):
        try:
            post_data = request.data
            # TODO VALIDATE OTP

            status, response = CustomUserRepository.create_new_user(post_data)
            if status:
                return JSONResponse({'code': 200, 'response': response})
            return JSONResponse({'code': 400, 'response': response})
        except Exception as e:
            logger.error('Error Creating User - User Controller  ', e)
            return JSONResponse({'code': 500, 'response': 'Something Went Wrong'})
       

    @staticmethod
    @api_view(['GET'])
    @login_required
    def me(request):
        try:
            user = request.user
            ##TODO Decide on Currect Serializer based on Requirements
            serialized_user = CustomUserSerializer(user).data
            return JSONResponse({'code': 200, 'response': serialized_user})
        except Exception as e:
            logger.error('Getting User - User Controller :: ' + str(e), exc_info = 1)
            return JSONResponse({'code': 500, 'response': 'Something Went Wrong'})
        

    @staticmethod
    @login_required
    @api_view(['GET'])
    def logout(request):
        user = request.user
        if user.is_authenticated:
            user.auth_token.delete()
        return JSONResponse({'code': 200, 'response': 'Logout Successful'})

    @staticmethod
    @api_view(['POST'])
    def login(request):
        post_data = request.data
        # print(post)
        user_name = post_data.get('username')
        password = post_data.get('password')
        if not user_name or not password:
            return JSONResponse({'code': 400, 'response': 'Invalid Params'})
        user  =  get_or_none(CustomUser, email__iexact = user_name )            
        if not user:
            return JSONResponse({'code':403,'response':'Invalid Credentials'})
        user_mail = user.email 
        authentication = authenticate(username = user_mail, password = password)
        logger.info('Authenticating ' + user_mail, exc_info = 0)
        if not authentication: 
            return JSONResponse({'code':403,'response':'Invalid Credentials'})
        if user.is_active == False:
            return JSONResponse({'code':403,'response':{'rejected':'Account Blocked'}})             
        token = authtoken.models.Token.objects.get_or_create(user=user)     
        return JSONResponse({'code':200,'response':str(token[0])})