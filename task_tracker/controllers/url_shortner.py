from rest_framework.decorators import api_view
from task_tracker.models import ShortUrl
from task_tracker.helpers.base_utils import *
from task_tracker.helpers.model_search import *
import base62
from django.shortcuts import redirect

BASE_URL = 'http://0.0.0.0:8000/s/'

class ShortUrlController:

    @staticmethod
    @api_view(['POST'])
    def get_short_url(request):
        long_url = request.data.get('long_url')
        url_object = get_or_create(ShortUrl, long_url = long_url)
        url_object.save()
        encode = base62.encode(url_object.id)
        url_object.encode = encode
        url_object.save()
        short_url = BASE_URL + str(encode)
        return JSONResponse({'code': 200, 'response': short_url})

    
    @staticmethod
    def get_long_url(request, short_url):
        decode = base62.decode(short_url)
        url_object = get_or_none(ShortUrl, id = decode)
        return redirect(url_object.long_url)
        



