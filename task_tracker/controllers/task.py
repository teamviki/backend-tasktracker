from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
from task_tracker.repositories.task import *
from task_tracker.helpers.base_utils import JSONResponse
from task_tracker.serializer import TaskSerializer, TaskCommentSerializer
from datetime import datetime

class TaskController:

    @staticmethod
    @api_view(['GET'])
    @login_required
    def get_tasks(request, project_id):
        user = request.user
        task_list = TaskRepository(list = true, project__id = project_id).get_user_tasks()
        if task_list:
            return JSONResponse({'code': 200, 'response': TaskCommentSerializer(task_list, many = True).data})
        else:
            return JSONResponse({'code': 404, 'response': [], 'message': 'No Tasks Found' })

    
    @staticmethod
    @api_view(['POST'])
    @login_required
    def create_task(request):
        user = request.user
        post_data = request.data
        name = post_data.get('taskName')
        project_id = post_data.get('projectId')
        priority = post_data.get('priority')

        description = post_data.get('description')
        start_date = post_data.get('start_date')
        end_date = post_data.get('endDate')
        if end_date:
            end_date = datetime.strptime(end_date,"%Y-%m-%d")
        if start_date:
            start_date = datetime.strptime(start_date,"%Y-%m-%d")

        # is_existing_task = TaskRepository.check_task_exist_for_user(user, name)
        # if is_existing_task:
        #     return JSONResponse({'code': 403, 'message': 'Task already exists please select a different name'})
        
        status, message = TaskRepository.create_task(project_id, name, user,priority, description, start_date, end_date)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})


    @staticmethod
    @api_view(['PUT'])
    @login_required
    def update_task(request, task_id):
        user = request.user
        put_data = request.data
        name = put_data.get('name')
        description = put_data.get('description')
        status = put_data.get('status')
        priority = put_data.get('priority')

        start_date = put_data.get('start_date')
        end_date = put_data.get('end_date')
        if end_date:
            end_date = datetime.strptime(end_date,"%Y-%m-%d")
        if start_date:
            start_date = datetime.strptime(start_date,"%Y-%m-%d")
        is_existing_task = TaskRepository.check_task_exist_for_user(user, id = task_id)
        if not is_existing_task:
            return JSONResponse({'code': 404, 'message': 'Task not found'})
        status, message = TaskRepository(owner = user, id = task_id).update_task(name, user, priority, description, status,start_date, end_date)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})


    
    @staticmethod
    @api_view(['DELETE'])
    @login_required
    def delete_task(request, task_id):
        user = request.user
        is_existing_task = TaskRepository.check_task_exist_for_user(user, id = task_id)
        if not is_existing_task:
            return JSONResponse({'code': 404, 'message': 'Task not found'})
        status, message = TaskRepository.delete_task(task_id)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})
