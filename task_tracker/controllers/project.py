from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
from task_tracker.repositories.project import *
from task_tracker.helpers.base_utils import JSONResponse
from task_tracker.serializer import ProjectSerializer, ProjectTaskSerializer
from datetime import datetime

class ProjectController:

    @staticmethod
    @api_view(['GET'])
    @login_required
    def get_projects(request):
        user = request.user
        project_list = ProjectRepository(many = True, user = user).get_user_projects()
        if project_list:
            return JSONResponse({'code': 200, 'response': ProjectTaskSerializer(project_list, many = True).data})
        else:
            return JSONResponse({'code': 404, 'response': [], 'message': 'No Projects Found' })

    
    @staticmethod
    @api_view(['POST'])
    @login_required
    def create_project(request):
        user = request.user
        post_data = request.data
        print(post_data)
        name = post_data.get('name')
        description = post_data.get('description')
        start_date = post_data.get('start_date')
        end_date = post_data.get('end_date')
        if end_date:
            end_date = datetime.strptime(end_date,"%Y-%m-%d")
        if start_date:
            start_date = datetime.strptime(start_date,"%Y-%m-%d")

        is_existing_project = ProjectRepository.check_project_exist_for_user(user, name)
        if is_existing_project:
            return JSONResponse({'code': 403, 'message': 'Project already exists please select a different name'})
        
        status, message = ProjectRepository.create_project(user, name, description, start_date, end_date)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})


    @staticmethod
    @api_view(['PUT'])
    @login_required
    def update_project(request, project_id):
        user = request.user
        put_data = request.data
        name = put_data.get('name')
        description = put_data.get('description')
        status = put_data.get('status')
        start_date = put_data.get('start_date')
        end_date = put_data.get('end_date')
        if end_date:
            end_date = datetime.strptime(end_date,"%Y-%m-%d")
        if start_date:
            start_date = datetime.strptime(start_date,"%Y-%m-%d")

        is_existing_project = ProjectRepository.check_project_exist_for_user(user, id = project_id)
        if not is_existing_project:
            return JSONResponse({'code': 404, 'message': 'Project not found'})
        
        status, message = ProjectRepository(user = user, id = project_id).update_project(name, description, status,start_date, end_date)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})


    
    @staticmethod
    @api_view(['DELETE'])
    @login_required
    def delete_project(request, project_id):
        user = request.user
        is_existing_project = ProjectRepository.check_project_exist_for_user(user, id = project_id)
        if not is_existing_project:
            return JSONResponse({'code': 404, 'message': 'Project not found'})
        status, message = ProjectRepository.delete_project(project_id)
        if status:     
            return JSONResponse({'code': 200, 'response': message})
        return JSONResponse({'code': 400, 'response': '', message: 'Bad Request'})
