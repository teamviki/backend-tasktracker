from django.urls import include, path, re_path
from . import views

urlpatterns = [
    path('user/', views.UserController.me, name = 'get_profile'),
    path('login/', views.UserController.login, name = 'get_profile'),
    path('logout/', views.UserController.logout, name = 'get_profile'),
    path('signup/', views.UserController.signup, name = 'get_profile'),

    path('projects/', views.ProjectController.get_projects, name = 'get_profile'),
    path('project/create', views.ProjectController.create_project, name = 'get_profile'),
    path('project/update/<int:project_id>', views.ProjectController.update_project, name = 'get_profile'),
    path('project/delete/<int:project_id>', views.ProjectController.delete_project, name = 'get_profile'),
    
    path('tasks/<int:project_id>', views.TaskController.get_tasks, name = 'get_profile'),    
    path('task/create', views.TaskController.create_task, name = 'get_profile'),
    path('task/update/<int:task_id>', views.TaskController.update_task, name = 'get_profile'),
    path('task/delete/<int:task_id>', views.TaskController.delete_task, name = 'get_profile'),
     path('tasks/<int:project_id>', views.TaskController.get_tasks, name = 'get_profile'), 

    path('document/create', views.DocumentController.upload_document, name = 'get_profile'),
    path('document/delete/<int:document_id>', views.DocumentController.delete_document, name = 'get_profile'),
]