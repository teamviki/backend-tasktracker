from task_tracker.models import Document
from task_tracker.helpers.model_search import *
from task_tracker.helpers.base_utils import *

import logging

logger = logging.getLogger()

class DocumentRepository:
    object = None
    def __init__(self, object = None, many = False, list = None, *args, **kwargs):
        if not object:
            object = get_or_none(Document, *args, **kwargs)
        self.object = object
        if many:
            if not list:
                list = filter_or_none(Document, *args, **kwargs)
        self.list = list



    @staticmethod
    def create_document(type, type_id, user, file):
        try:
            task = Document.objects.create(
                type = type,
                type_id = type_id,
                user = user,
                file = file,
            )
            return True, 'Document Created Successfully'
        except Exception as e:
            logger.error('Something Went Wrong while Creating Document  :: ' + str(e), exc_info = 1)
            raise Exception('Something Went Wrong while creating Document')

    @staticmethod
    def delete_document(id):
        try:
            object = get_or_none(Document, id = id)
            object.delete()
            return True, 'Document Deleted Successfully'
        except expression as e:
                logger.error('Something Went Wrong while Deleting Document  :: ' + str(e), exc_info = 1)
                raise Exception('Something Went Wrong while Deleting Document')
                