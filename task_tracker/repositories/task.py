from task_tracker.models import *
from task_tracker.helpers.model_search import *
from task_tracker.helpers.base_utils import *

import logging

logger = logging.getLogger()

class TaskRepository:
    object = None
    def __init__(self, object = None, many = False, list = None, *args, **kwargs):
        if not object:
            object = get_or_none(Task, *args, **kwargs)
        self.object = object
        if many:
            if not list:
                list = filter_or_none(Task, *args, **kwargs)
        self.list = list

    def get_project_tasks(self, status = None):
        list = self.list
        if list and status:
            list.filter(status = status)
        return list

    @staticmethod
    def check_task_exist_for_user(owner, name = None, id = None):
        status = None
        if name:
            status = filter_or_none(Task, owner = owner, name = name)
        elif id: 
            status = filter_or_none(Task, owner = owner, id = id)
        if status:
            return True
        return False

    @staticmethod
    def create_task(project_id, name, owner, priority, description, start_date, end_date):
        try:
            project = get_or_none(Project, id = project_id)
            if not project:
                return False, 'Project not found'
            task = Task.objects.create(
                project = project,
                name = name,
                owner = owner,
                priority = priority,
                description = description,
                start_date = start_date,
                end_date = end_date
            )
            return True, 'Task Created Successfully'
        except Exception as e:
            logger.error('Something Went Wrong while Creating Task  :: ' + str(e), exc_info = 1)
            raise Exception('Something Went Wrong while creating Task')
       

    def update_task(self, name, owner, priority, description, status, start_date, end_date):
        try:          
            object = self.object
            if name:
                object.name = name

            if owner: 
                object.owner = owner

            if priority:
                object.priority = priority

            if description:
                object.description = description
                
            if status:
                object.status = status
                object.status_date  = datetime.now()

            if start_date:
                object.start_date = start_date

            if end_date:
                object.end_date = end_date

            object.save()
            return True, 'Task Updated Successfully'
        except expression as e:
            logger.error('Something Went Wrong while Updating Task  :: ' + str(e), exc_info = 1)
            raise Exception('Something Went Wrong while Updating Task')

    @staticmethod
    def delete_task(id):
        try:
            object = get_or_none(Task, id = id)
            object.delete()
            return True, 'Object Deleted Successfully'
        except expression as e:
                logger.error('Something Went Wrong while Deleting Task  :: ' + str(e), exc_info = 1)
                raise Exception('Something Went Wrong while Deleting Task')
                