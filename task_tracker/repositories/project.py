from task_tracker.models import Project
from task_tracker.helpers.model_search import *
from task_tracker.helpers.base_utils import *

import logging

logger = logging.getLogger()

class ProjectRepository:
    object = None
    def __init__(self, object = None, many = False, list = None, *args, **kwargs):
        if not object and not many:
            object = get_or_none(Project, *args, **kwargs)
        self.object = object
        if many:
            if not list:
                list = filter_or_none(Project, *args, **kwargs)
        self.list = list

    def get_user_projects(self, status = None):
        list = self.list
        if list and status:
            list.filter(status = status)
        return list

    @staticmethod
    def check_project_exist_for_user(user, name = None, id = None ):
        status = None
        if name:
            status = filter_or_none(Project, user = user, name = name)
        elif id: 
            status = filter_or_none(Project, user = user, id = id)
        if status:
            return True
        return False

    @staticmethod
    def create_project(user,name, description, start_date, end_date):
        try:
            project = Project.objects.create(
                user = user,
                name = name,
                description = description,
                start_date = start_date,
                end_date = end_date
            )
        
            return True, 'Project Created Successfully'
        except expression as e:
            logger.error('Something Went Wrong while Creating Project  :: ' + str(e), exc_info = 1)
            raise Exception('Something Went Wrong while creating Project')
       

    def update_project(self, name, description, status, start_date, end_date):
        try:          
            object = self.object
            if name:
                object.name = name
            if description:
                object.description = description
            if status:
                object.status = status
                object.status_date  = datetime.now()
            if start_date:
                object.start_date = start_date
            if end_date:
                object.end_date = end_date
            object.save()
            return True, 'Project Updated Successfully'
        except expression as e:
            logger.error('Something Went Wrong while Updating Project  :: ' + str(e), exc_info = 1)
            raise Exception('Something Went Wrong while Updating Project')

    @staticmethod
    def delete_project(id):
        try:
            object = get_or_none(Project, id = id)
            object.delete()
            return True, 'Object Deleted Successfully'
        except expression as e:
                logger.error('Something Went Wrong while Deleting Project  :: ' + str(e), exc_info = 1)
                raise Exception('Something Went Wrong while Deleting Project')
                