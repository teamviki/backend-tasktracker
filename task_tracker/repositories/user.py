from task_tracker.models import *
from task_tracker.helpers.model_search import *
from task_tracker.helpers.base_utils import logger


class CustomUserRepository():
    object = None
    list = None
    def __init__(self, object = None, many = False, list = None, *args, **kwargs):
        if not object:
            object = get_or_none(CustomUser, *args, **kwargs)
        self.object = object
        if many:
            if not list:
                list = filter_or_none(CustomUser, *args, **kwargs)
            self.list = list
    
    @staticmethod
    def check_user_exists(email, mobile_number):
        existing_email = get_or_none(CustomUser, email = email)
        if existing_email:
            return True, "Email Already Registered"
        return False, "New User"
        pass

    @staticmethod
    def create_new_user(parameter_list):
        try:
            email = parameter_list.get('email')
            password = parameter_list.get('password')
            full_name = parameter_list.get('full_name')
            existing_user = get_or_none(CustomUser, email__iexact = email )
            if existing_user:
                return False, 'Email Already Registered'
                
            user = CustomUser.objects.create_user(
                email = email,
                password = password,
                full_name = full_name,
            )
            user.save()
            group_name = parameter_list.get('group_nam')
            if group_name:
                group = get_or_none(Group, name = group_name)
                group.user_set.add(group)
                group.save()
            return True, 'User Created Successfully'
        except Exception as e:
            logger.error('Create User - Repository Error', str(e))
            raise
