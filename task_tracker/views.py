from django.shortcuts import render
from task_tracker.controllers.user import UserController
from task_tracker.controllers.project import ProjectController
from task_tracker.controllers.task import TaskController
from task_tracker.controllers.document import DocumentController

from task_tracker.controllers.url_shortner import *

# Create your views here.
