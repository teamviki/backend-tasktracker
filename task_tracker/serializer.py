from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework import serializers
from task_tracker.helpers.model_search import *
from task_tracker.models import *

class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            'user_id',
            'full_name',
            'email',
            'profile_photo',
            'is_staff',
            'is_active',
            'created_date',
            'updated_date'
        )

class ProjectSerializer(ModelSerializer):
    user = CustomUserSerializer()
    class Meta:
        model = Project
        fields = (
            'id',
            'name',
            'project_id',
            'user',
            'description',
            'status',
            'start_date',
            'end_date',
        )
    


class TaskSerializer(ModelSerializer):
    # project = ProjectSerializer()
    owner = CustomUserSerializer()
    actors = CustomUserSerializer(many = True)

    class Meta:
        model = Task
        fields = (
            'id',
            'project',
            'name',
            'owner',
            'actors',
            'priority',
            'description',
            'status',
            'start_date',
            'end_date',
        )


class DocumentSerializer(ModelSerializer):
    user = CustomUserSerializer()
    class Meta:
        model = Document
        fields = (
            'id',
            'type',
            'type_id',
            'file',
            'user',
            'status'
        )


class CommentSerializer(ModelSerializer):
    documents = SerializerMethodField()

    def get_documents(self, object):
        documents = filter_or_none(Document, type = 'COMMENT', type_id = object.id)
        if documents:
            return DocumentSerializer(documents, many = true)
        return []

    class Meta:
        model = Comment
        fields = (
            'task',
            'value',
            'user',
            'status',
        )



class TaskCommentSerializer(ModelSerializer):
    comments = SerializerMethodField()
    def get_comments(self, object):
        comments = filter_or_none(Comment, task = task)
        if comments:
            return CommentSerializer(comments, many = True).data
        return []
    class Meta:
        model = Task
        fields = (
            'project',
            'name',
            'owner',
            'actors',
            'priority',
            'description',
            'status',
            'start_date',
            'end_date',
            'comments'
        )
    

class TaskDocumentSerializer(ModelSerializer):
    documents = SerializerMethodField()

    def get_documents(self, object):
        documents = filter_or_none(Document, type = 'TASK', type_id = object.id )
        if documents:
            return DocumentSerializer(documents, many = True).data
        return []
    class Meta:
        model = Task
        fields = (
            'id',
            'project',
            'name',
            'owner',
            'actors',
            'priority',
            'description',
            'status',
            'start_date',
            'end_date',
            'documents',
        )


class ProjectTaskSerializer(ModelSerializer):
    tasks = SerializerMethodField()

    def get_tasks(self, object):
        tasks = filter_or_none(Task, project = object)
        if tasks: 
            return TaskDocumentSerializer(tasks, many = True).data
        return None
    
    class Meta:
        model = Project
        fields = (
            'id',
            'name',
            'project_id',
            'tasks',
            'description',
            'status',
            'start_date',
            'end_date',
        )