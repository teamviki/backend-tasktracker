from django.db.models import *

def get_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None

def filter_or_none(model, *args, **kwargs):
    try:
        return model.objects.filter(*args, **kwargs)
    except model.DoesNotExist:
        return None

def get_or_none_locked(model, *args, **kwargs):
    try:
        return model.objects.select_for_update().get(*args, **kwargs)
    except model.DoesNotExist:
        return None

def filter_or_none_locked(model, *args, **kwargs):
    try:
        return model.objects.select_for_update().filter(*args, **kwargs)
    except model.DoesNotExist:
        return None
        
        
def get_count_and_sum(model_object, sum_column, *args, **kwargs):
    filtered_list=model_object.filter( *args, **kwargs)
    count  = filtered_list.count()
    sum = filtered_list.aggregate(Sum(sum_column)).get(sum_column + '__sum')
    return {'count':count, 'sum':sum}
        
       
def get_model_count(model_object, *args, **kwargs):
    return model_object.filter( *args, **kwargs).count()
          
       
def get_model_sum(model_object, sum_column, *args, **kwargs):
    return model_object.filter( *args, **kwargs).aggregate(Sum(sum_column)).get(sum_column + '__sum')
 

def get_model_avg(model_object, avg_column, *args, **kwargs):
    return model_object.filter( *args, **kwargs).aggregate(Avg(avg_column)).get(avg_column + '__avg')  


def get_model_max(model_object, max_column, *args, **kwargs):
    return model_object.filter( *args, **kwargs).aggregate(Max(max_column)).get(max_column + '__max')    

def get_model_min(model_object, min_column, *args, **kwargs):
    return model_object.filter( *args, **kwargs).aggregate(Min(min_column)).get(min_column + '__min')    

def get_or_create(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return model(*args, **kwargs)
      

def is_model_value_unique(model_name,*args, **kwargs):
    #try:
       status =   model_name.objects.filter(*args, **kwargs).exists()
       if status:
            return False
       return True
  #  except:
     #   raise ValueError('Invalid Model Field')