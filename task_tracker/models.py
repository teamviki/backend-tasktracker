from django.db import models
from django.db.models import Model, CharField, IntegerField, Q, Sum, Count, Avg,Func, FloatField, ForeignKey, ManyToManyField, TextField, DateTimeField, FileField
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField, RandomCharField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, User, PermissionsMixin, Group, GroupManager
from simple_history.models import HistoricalRecords
import random
from datetime import datetime

# Create your models here.


# CUSTOM USER MANAGER
class CustomUserManager(BaseUserManager):
	def create_user(self,									
					email,
					password,
					full_name=None,
					):	
		if not email:
			raise ValueError('Enter a valid email address')		
		user = self.model(
						full_name = full_name,
						email = email,
						)	
		
		user.set_password(password)
		user.save(using=self._db)
		return user
	
	
	def create_superuser(self, email, password):
		"""
		Creates and saves a superuser with the given email, date of
		birth and password.
		"""
		user = self.create_user(email,
			password=password,
		)		
		user.is_admin = True
		#user.is_a_staff = True
		user.save(using=self._db)
		return user

def generate_user_id():
	user_id = "".join([random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVXYZ') for i in range(10)])
	return user_id	

def get_profile_photo_filepath_with_name(instance, name):
    date = datetime.strftime(datetime.now(), "%Y/%m/%d/")
    ext = name.split('.')[-1]
    return 'profile_photo/'+ date +instance.name+"."+ext


def get_document_filepath_with_name(instance, name):
    date = datetime.strftime(datetime.now(), "%Y/%m/%d/")
    ext = name.split('.')[-1]
    return 'document/'+ date +"."+ext

def generate_16_digit_alphanumeric_id():
	user_id = "".join([random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVXYZ') for i in range(10)])
	return user_id	

# CUSTOM USER
class CustomUser(AbstractBaseUser, PermissionsMixin):
    gender_choices = (('MALE', 'MALE'),
                        ('FEMALE', 'FEMALE'),
                        ('OTHER', 'OTHER'))

    user_id = CharField(max_length=255, editable=False,default=generate_user_id, unique=True, null=True)
    full_name = CharField(max_length=700, null=True)
    email = models.EmailField(verbose_name='email address', max_length=255,   unique=True, null=True)
    email_verification = models.BooleanField(default=False)
    profile_photo = models.ImageField(null = True, blank = True, upload_to = get_profile_photo_filepath_with_name)
    is_staff = models.BooleanField(default=False, verbose_name='Staff')
    is_active = models.BooleanField(default=True, verbose_name='Active')
    is_admin = models.BooleanField(default=False, verbose_name='Admin')
    history     = HistoricalRecords()	
    created_date= CreationDateTimeField(null=True)
    updated_date= ModificationDateTimeField(null=True)

    USERNAME_FIELD = 'email'
    objects = CustomUserManager()

  
        
    def __str__(self): 		
        return self.email

    def has_perm(self, perm, obj=None):

        if self.is_admin:		
            return True
        return perm in self.get_group_permissions()
        
    def has_module_perms(self, app_label):
        return True

    def has_group(self,group_name):
        user = get_or_none(CustomUser, email = self.email, groups__name = group_name)
        if not user:
            return False
        return True

    def get_first_name(self):
        list = self.full_name.split(' ')
        if list :
                return list[0]
        return None

    def group_list(self):
        return self.groups.all()

    def primary_group(self):
        groups=[item.name for item in self.groups.all()]
        if not groups:
            return 'GUEST'
        return groups[0]

    def get_last_name(self):
        list = self.full_name.split(' ')
        if list :
                return list[len(list) - 1]
        return None


class Project(Model):
    name = CharField(max_length = 200)
    project_id = CharField(max_length = 16, default = generate_16_digit_alphanumeric_id, unique = True, db_index = True)
    user = ForeignKey('CustomUser', on_delete = models.CASCADE, related_name = 'project_owner')
    description = TextField(null = True, blank = True)
    status = CharField(max_length = 15, default = 'CREATED')
    status_date = DateTimeField(auto_now_add=True)
    start_date = DateTimeField(null = True, blank = True)
    end_date = DateTimeField(null = True, blank = True)

    created_date = CreationDateTimeField()
    updated_date = ModificationDateTimeField()

    def __str__(self):
            return self.name

    class Meta:
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'


class Task(Model):
    project = ForeignKey('Project', on_delete = models.CASCADE, related_name = 'task_project')
    name = CharField(max_length = 200)
    owner = ForeignKey('CustomUser', on_delete = models.CASCADE)
    actors = ManyToManyField('CustomUser', related_name= "task_actors")
    priority = CharField(max_length = 15,null= True, blank = True)
    description = TextField(null = True, blank = True)
    status = CharField(max_length = 15, default = 'CREATED')
    status_date = DateTimeField(auto_now_add=True)
    # tags = 
    start_date = DateTimeField(null = True, blank = True)
    end_date = DateTimeField(null = True, blank = True)

    created_date = CreationDateTimeField()
    updated_date = ModificationDateTimeField()

    def __str__(self):
            return self.name
            
    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'

    
class Comment(Model):
    task =  ForeignKey('Task', on_delete = models.CASCADE, related_name = 'task_comment')
    value = TextField()
    user = ForeignKey('CustomUser', models.CASCADE, related_name = 'comment_owner')
    status = CharField(max_length = 15, default = 'ACTIVE')
    status_date = DateTimeField(auto_now_add=True)
    created_date = CreationDateTimeField()
    updated_date = ModificationDateTimeField()

    def __str__(self):
            return self.id + ' ' + self.task.name
            
    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'


  
class Document(Model):
    type = CharField(max_length = 25, default = 'COMMENT')
    type_id = CharField(max_length = 50)
    file = FileField(upload_to=get_document_filepath_with_name)
    user = ForeignKey('CustomUser', models.CASCADE, related_name = 'document_owner')
    status = CharField(max_length = 15, default = 'ACTIVE')
    status_date = DateTimeField(auto_now_add=True)
    created_date = CreationDateTimeField()
    updated_date = ModificationDateTimeField()

    def __str__(self):
            return self.type
            
    class Meta:
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'



class ShortUrl(Model):
    encode = CharField(max_length = 20)
    long_url = CharField(max_length = 500)
    created_date = CreationDateTimeField()
    updated_date = ModificationDateTimeField()

    def __str__(self):
        return self.encode
            
    class Meta:
        verbose_name = 'ShortUrl'
        verbose_name_plural = 'ShortUrls'
